﻿#include <iostream>
#include <typeinfo>

using namespace std;

template <class T>
class Stack
{
private:
	int top;
	T* DArray;
	int SStack;
	T elem;
		
public:
	Stack (int size)	//Конструктор
	{
		DArray = new T[size]; 
		top = 0;
		SStack = size;
	}

	~Stack()			//Деструктор
	{
		delete[] DArray;
		DArray = nullptr;
	}

	void push()	//Добавление элемента в стек
	{
		int i = SStack;
		cout << "\nEnter the values of the elements:" << endl;
		if (top < SStack)
		{
			while(true & i >= 1)
			{
				cout << i << " - "; cin >> elem;
				if (cin.fail())
				{
					cin.clear();
					cin.ignore(32767, '\n');
				}
				else
				{
					DArray[top++] = elem;
					i--;
				}
			}
		}
		
	}

	void push(T el)	//Добавление элемента в стек
	{
		if (top < SStack) { DArray[top++] = el; }
		else
		{
			cout << "Stack is full!"<<endl;
		}
	}

	T pop()			//Извлечение элемента из стека
	{if (top>=0) return DArray[--top];}
};


int main()
{
	int Size; 
	int num = 1;

	do
	{cout << "Enter size Stack: "; cin >> Size;}
	while (Size <= 0);
	// Создание стека
	Stack<float> MyStack(Size);
	
	//Заполнение стека
	MyStack.push();
	MyStack.push(5); //скажет что стек полон!
	
	//Извлечение и вывод на экран элементов стека
	cout << endl;
	
	
	
	for (int index = Size-1; index >= 0; index--)
	{
		cout << num++ << ": " << MyStack.pop() << endl;
	}
	
	return 0;
}